let pokedexOl = document.getElementById("pokedex");

const obtainPokemon = async(id) => {
    fetch(`https://pokeapi.co/api/v2/pokemon/${id}/`)
    .then((resp) => resp.json())
    .then((data) => {
        createCard(data)
    });

}

const createPokemonsBynNumber = (dato) => {
    for (i=1;i<=dato; i++){
        obtainPokemon(i);
    }
}

const createCard = (pokemon) => {

    const carta = document.createElement('li');
    carta.classList.add("card");

    const imagen = document.createElement('img');
    imagen.src=`${pokemon.sprites.front_default}`;
    imagen.classList.add("card-image");

    const nombre = document.createElement('h2');
    nombre.textContent=`${pokemon.name}`
    nombre.classList.add('card-title')

    const id = document.createElement('p');
    id.textContent=`#${pokemon.id}`;
    id.classList.add('card-subtitle')

    const tipo = document.createElement('p');
    tipo.textContent=pokemon.types.map((dato) => dato.type.name).join(' - ');

   carta.appendChild(imagen);
   carta.appendChild(nombre);
   carta.appendChild(id);
   carta.appendChild(tipo);
    
   pokedexOl.appendChild(carta);
}

createPokemonsBynNumber(150);


