pokedexOl = document.getElementById("pokedex");
pokedexOl.className="container";

const listPokemon = [];

const  getCharacters = async() => {
    for (id=1; id<=150;id++){
    let response = await fetch(` https://pokeapi.co/api/v2/pokemon/${id}`);
    let data = await response.json();
    let pokemon = (data); 
    console.log(pokemon);
    
    listPokemon.push(pokemon);
    console.log(pokemon);
   
    const mapeoPokemon = listPokemon.map((pokemones) => ({
        name : pokemones.name,
        photo : pokemones.sprites.front_default,
        id : pokemones.id,
        types : pokemones.types.map((datos) => datos.type.name).join(' - ')
    }))
    mostrarPersonaje(mapeoPokemon);
}
console.log(listPokemon);
}

const mostrarPersonaje = (personajes) => {
    const myHtml = personajes.map((pokemones) => 
    `<li class="card">
    <img src=${pokemones.photo} alt="error" class="card-image"/>
    <h2 class="card-title">${pokemones.name}</p>
    <p class="card-subtitle">#${pokemones.id}</p>
    <h4>${pokemones.types}</h4>
    </li>
    `).join("");
pokedexOl.innerHTML = myHtml;
}

getCharacters()
